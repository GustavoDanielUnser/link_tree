package br.edu.unisep.linktree.ui.link.insert

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import br.edu.unisep.linktree.databinding.DialogNewLinkBinding
import br.edu.unisep.linktree.domain.dto.NewLinkDto
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

typealias OnSaveLink = (NewLinkDto) -> Unit

class InsertLinkDialog : BottomSheetDialogFragment() {

    private lateinit var binding: DialogNewLinkBinding
    lateinit var onSave: OnSaveLink

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogNewLinkBinding.inflate(inflater)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupView()
    }

    private fun setupView() {
        binding.btnSave.setOnClickListener { save() }
    }

    private fun save() {
        val link = NewLinkDto(
            name = binding.etLinkTitle.text.toString(),
            description = binding.etDescription.text.toString(),
            url =  binding.etUrl.text.toString(),
        )

        onSave(link)
        dialog?.dismiss()
    }

    companion object {
        @JvmStatic
        fun createAndShow(fragmentManager: FragmentManager, onSave: OnSaveLink) {
            val dialog = InsertLinkDialog()
            dialog.onSave = onSave
            dialog.show(fragmentManager, "insert-link")
        }
    }
}