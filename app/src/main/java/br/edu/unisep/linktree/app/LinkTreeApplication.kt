package br.edu.unisep.linktree.app

import android.app.Application
import br.edu.unisep.linktree.data.db.LinkDb

class LinkTreeApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        LinkDb.initialize(this)
    }
}