package br.edu.unisep.linktree.data.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import br.edu.unisep.linktree.data.dao.LinkDao
import br.edu.unisep.linktree.data.entity.Link

@Database(
    entities = [Link::class],
    version = 1
)
abstract class LinkDb : RoomDatabase(){

    abstract fun linkDao() : LinkDao

    companion object {
        private lateinit var mInstance: LinkDb
        val instance: LinkDb
            get() = mInstance

        fun initialize(context: Context) {
            mInstance = Room.databaseBuilder(context,
                LinkDb::class.java, "db_link").build()
        }
    }
}