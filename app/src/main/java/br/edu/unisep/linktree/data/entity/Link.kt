package br.edu.unisep.linktree.data.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class Link(
    val name: String,
    val description: String,
    val url: String
) {
    @PrimaryKey(autoGenerate = true) var id: Int? = null
}