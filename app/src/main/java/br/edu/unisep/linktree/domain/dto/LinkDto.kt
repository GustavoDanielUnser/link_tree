package br.edu.unisep.linktree.domain.dto

import java.io.Serializable

data class LinkDto(
    val id: Int,
    val name: String,
    val description: String,
    val url: String,
) : Serializable