package br.edu.unisep.linktree.data.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import br.edu.unisep.linktree.data.entity.Link

@Dao
interface LinkDao {

    @Insert
    suspend fun save(link: Link)

    @Query( "select * from link")
    suspend fun findAll(): List<Link>

    @Query("SELECT * FROM link WHERE name LIKE '%' || :name || '%'")
    suspend fun findByName(name: String?): List<Link>
}