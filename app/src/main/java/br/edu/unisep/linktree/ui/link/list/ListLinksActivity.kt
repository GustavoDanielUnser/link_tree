package br.edu.unisep.linktree.ui.link.list

import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.SearchView
import androidx.activity.viewModels
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager.VERTICAL
import br.edu.unisep.linktree.R
import br.edu.unisep.linktree.databinding.ActivityListLinksBinding
import br.edu.unisep.linktree.domain.dto.LinkDto
import br.edu.unisep.linktree.domain.dto.NewLinkDto
import br.edu.unisep.linktree.ui.link.insert.InsertLinkDialog
import br.edu.unisep.linktree.ui.link.list.adapter.LinkAdapter
import br.edu.unisep.linktree.ui.link.list.contract.ListLinksContract
import br.edu.unisep.linktree.ui.link.list.viewmodel.ListLinkViewModel

class ListLinksActivity : AppCompatActivity() {

    private val binding by lazy {
        ActivityListLinksBinding.inflate(layoutInflater)
    }

    private val viewModel by viewModels<ListLinkViewModel>()

    private lateinit var adapter: LinkAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        setupView()
        setupObservers()

        viewModel.findAll()
    }

    private fun setupView() {
        adapter = LinkAdapter(::onLinkSelected)

        binding.rvLinks.adapter = adapter
        binding.rvLinks.layoutManager = LinearLayoutManager(this, VERTICAL, false)
        binding.rvLinks.addItemDecoration(
            DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        )
    }

    private fun setupObservers() {
        viewModel.links.observe(this) {
            adapter.links = it
        }

        viewModel.saveResult.observe(this) {
            viewModel.findAll()
        }
    }

    private fun onLinkSelected(link: LinkDto) {
        intent = Intent(Intent.ACTION_VIEW).setData(Uri.parse(link.url))
        startActivity(intent)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_list, menu)
        val searchItem = menu.findItem(R.id.search)
        if (searchItem != null) {
            val searchView = searchItem.actionView as SearchView
            searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
                override fun onQueryTextSubmit(txt: String?): Boolean {
                    txt?.let {
                        viewModel.filter(it)
                    }
                    return true
                }

                override fun onQueryTextChange(txt: String?): Boolean {
                    txt?.let {
                        if ( it != "") {
                            viewModel.filter(it)
                        } else {
                            viewModel.findAll()
                        }
                    }
                    return true
                }
            })
        }

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.mnNew) {
            InsertLinkDialog.createAndShow(supportFragmentManager, ::onSave)
            return true
        }

        return false
    }

    private fun onSave(link: NewLinkDto) {
        viewModel.save(link)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (viewModel.hasSaved) {
            setResult(ListLinksContract.RESULT_LINKS_CREATED)
        }
    }
}