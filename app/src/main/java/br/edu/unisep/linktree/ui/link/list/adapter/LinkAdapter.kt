package br.edu.unisep.linktree.ui.link.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import br.edu.unisep.linktree.R
import br.edu.unisep.linktree.databinding.ItemLinkBinding
import br.edu.unisep.linktree.domain.dto.LinkDto

class LinkAdapter (private val onItemSelected: (LinkDto) -> Unit) : RecyclerView.Adapter<LinkAdapter.LinkViewHolder>() {

    var links: List<LinkDto> = listOf()
        set(value) {
            field = value
            this.notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LinkViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val itemBinding = ItemLinkBinding.inflate(layoutInflater, parent, false)
        return LinkViewHolder(itemBinding)
    }

    override fun onBindViewHolder(holder: LinkViewHolder, position: Int) {
        holder.bind(links[position], onItemSelected)
    }

    override fun getItemCount() = links.size

    class LinkViewHolder(private val itemBinding: ItemLinkBinding): RecyclerView.ViewHolder(itemBinding.root) {
        fun bind(link: LinkDto, onItemSelected: (LinkDto) -> Unit) {
            itemBinding.tvLinkTitle.text = link.name
            itemBinding.tvLinkDesc.text = link.description

            itemBinding.root.setOnClickListener {
                onItemSelected(link)
            }
        }
    }
}