package br.edu.unisep.linktree.ui.link.list.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import br.edu.unisep.linktree.domain.dto.LinkDto
import br.edu.unisep.linktree.domain.dto.NewLinkDto
import br.edu.unisep.linktree.domain.repository.LinkRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ListLinkViewModel : ViewModel() {

    private val _links = MutableLiveData<List<LinkDto>>()
    val links: LiveData<List<LinkDto>>
        get() = _links

    private val _saveResult = MutableLiveData<Unit>()
    val saveResult: LiveData<Unit>
        get() = _saveResult

    private var _hasSaved = false
    val hasSaved: Boolean
        get() = _hasSaved

    private val repository = LinkRepository()

    fun findAll() {
        viewModelScope.launch ( Dispatchers.IO ) {
            val list = repository.findAll()
            _links.postValue(list)
        }
    }

    fun filter(name: String) {
        viewModelScope.launch(Dispatchers.IO) {
            val list = repository.findByName(name)
            _links.postValue(list)
        }
    }

    fun save(link: NewLinkDto) {
        viewModelScope.launch(Dispatchers.IO) {
            repository.save(link)
            _hasSaved = true
            _saveResult.postValue(Unit)
        }
    }

}