package br.edu.unisep.linktree.domain.dto

data class NewLinkDto(
    val name: String,
    val description: String,
    val url: String,
)
