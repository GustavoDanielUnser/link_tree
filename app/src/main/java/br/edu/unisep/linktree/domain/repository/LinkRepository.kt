package br.edu.unisep.linktree.domain.repository

import br.edu.unisep.linktree.data.dao.LinkDao
import br.edu.unisep.linktree.data.db.LinkDb
import br.edu.unisep.linktree.data.entity.Link
import br.edu.unisep.linktree.domain.dto.LinkDto
import br.edu.unisep.linktree.domain.dto.NewLinkDto

class LinkRepository {

    private val dao: LinkDao by lazy {
        LinkDb.instance.linkDao()
    }

    suspend fun save(newLink: NewLinkDto) {
        val link = Link(
            newLink.name,
            newLink.description,
            newLink.url
        )
        dao.save(link)
    }

    suspend fun findAll(): List<LinkDto> {
        val result = dao.findAll()
        return result.map { link ->
            LinkDto(
                link.id!!,
                link.name,
                link.description,
                link.url,
            )
        }
    }

    suspend fun findByName(name: String): List<LinkDto> {
        val result = dao.findByName(name)
        return result.map { link ->
            LinkDto(
                link.id!!,
                link.name,
                link.description,
                link.url,
            )
        }
    }
}