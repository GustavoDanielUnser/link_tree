package br.edu.unisep.linktree.ui.link.list.contract

import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract
import br.edu.unisep.linktree.domain.dto.LinkDto
import br.edu.unisep.linktree.ui.link.list.ListLinksActivity

class ListLinksContract : ActivityResultContract<LinkDto, Int>() {

    override fun createIntent(context: Context, link: LinkDto) =
        Intent(context, ListLinksActivity::class.java).apply {
            putExtra("link", link)
        }

    override fun parseResult(resultCode: Int, intent: Intent?) = resultCode

    companion object {
        const val RESULT_LINKS_CREATED = 100
    }
}